package service

import scala.xml.XML
import java.text.SimpleDateFormat
import java.util.{ Date, Locale }

trait RssFeed {
  val link: String
  val title: String
  val desc: String
  val items: Seq[RssItem]
  override def toString = title + "\n" + desc + "\n**"

  def latest = items sortWith ((a, b) => a.date.compareTo(b.date) > 0) head
}
case class XmlRssFeed(title: String, link: String, desc: String, items: Seq[RssItem]) extends RssFeed

case class RssItem(title: String, link: String, desc: String, date: Date, guid: String) {
  override def toString = date + " " + title
}

/**
 * Created by rhegde on 9/12/2016.
 */
object RssReader {

  val dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH);

  def mailingListPosts: Seq[RssFeed] = {

    val xml = XML.load("http://www.meetup.com/hyscala/messages/archive/rss/hyscala-list+Meetup+Mailing+List/")

    for (channel <- xml \\ "channel") yield {
      val items = for (item <- (channel \\ "item")) yield {
        RssItem(
          (item \\ "title").text,
          (item \\ "link").text,
          (item \\ "description").text,
          dateFormatter.parse((item \\ "pubDate").text),
          (item \\ "guid").text
        )
      }
      XmlRssFeed(
        (channel \ "title").text,
        (channel \ "link").text,
        (channel \ "description").text,
        items.take(8))
    }
  }

}
