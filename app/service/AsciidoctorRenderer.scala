package service

import org.asciidoctor.Asciidoctor.Factory.create
import org.asciidoctor.OptionsBuilder
import org.asciidoctor.ast.DocumentHeader

import scala.io.Source._

/**
 * Created by rhegde on 9/29/2016.
 */
object AsciidoctorRenderer {

  def renderRemoteFile(path: String): String = {
    //val test = readDocumentHeader(path).getAttributes
    create().render(fromURL(path)("UTF-8").mkString, OptionsBuilder.options())

  }

  def readDocumentHeader(path: String) = {
    create().readDocumentHeader(fromURL(path)("UTF-8").mkString)
  }

}
